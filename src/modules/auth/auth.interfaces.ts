export interface ISignUpBody {
    name: string;
    email: string;
    password: string;
}

export interface ILoginBody {
    email: string;
    password: string;
}
