import { BadRequestException, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import * as bcrypt from 'bcrypt';
import { Repository } from 'typeorm';

import { EncryptionService } from '../encryption/encryption.service';
import { ILoginBody, ISignUpBody } from './auth.interfaces';
import { User } from './user.entity';

const MS_IN_SECONDS = 1000;

@Injectable()
export class AuthService {
    constructor(
        @InjectRepository(User)
        private usersRepository: Repository<User>,
        private readonly encryptionService: EncryptionService,
        private readonly jwtService: JwtService,
    ) {}

    public async signUp({ name, email, password }: ISignUpBody): Promise<User> {
        const user = new User();
        user.name = name;
        user.email = email;
        user.password = password;

        return this.usersRepository.save(user);
    }

    async login({ email, password }: ILoginBody): Promise<User & { token: string }> {
        const user = await this.usersRepository.findOne({ email });
        if (!Boolean(user)) {
            throw new BadRequestException('Incorrect user');
        }

        const isMatch = await bcrypt.compare(password, user.password);
        if (!isMatch) {
            throw new BadRequestException('Incorrect password');
        }

        const token = await this.signToken(email);

        return Object.assign(user, { token });
    }

    private async signToken(email: string): Promise<string> {
        const payload = {
            email,
            /**
             * При каждом обновлении токена время выпуска меняется, а нам надо сохранить исходное,
             * чтобы ориентироватьсяс на него при подсчете максимального времени жизни сесии
             */
            initialTokenIat: new Date().getTime() / MS_IN_SECONDS,
        };
        const encryptedPayload = this.encryptionService.encrypt(JSON.stringify(payload));
        const expiresIn = Math.ceil((60 * 1000 * 30) / MS_IN_SECONDS);

        return this.jwtService.sign(
            { payload: encryptedPayload },
            {
                expiresIn,
            },
        );
    }

    public async logUserAction(data: any) {
        return true;
    }

    public async getUsers() {
        return this.usersRepository.find();
    }
}
