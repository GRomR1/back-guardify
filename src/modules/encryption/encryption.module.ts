import { DynamicModule, Module, Provider } from '@nestjs/common';

import { EncryptionAsyncOptions, EncryptionOptionsFactory, ENCRYPTION_OPTIONS } from './encryption.entity';
import { EncryptionService } from './encryption.service';

/**
 * Модуль Шифрования.
 */
@Module({
    exports: [EncryptionService],
    providers: [EncryptionService],
})
export class EncryptionModule {
    /**
     * Вспомогательный метод.
     */
    private static createAsyncProviders (options: EncryptionAsyncOptions): Provider[] {
        if (options.useExisting || options.useFactory) {
            return [EncryptionModule.createAsyncOptionsProvider(options)];
        }

        return [
            EncryptionModule.createAsyncOptionsProvider(options),
            {
                provide: options.useClass,
                useClass: options.useClass,
            },
        ];
    }

    /**
     * Вспомогательный метод.
     */
    private static createAsyncOptionsProvider (options: EncryptionAsyncOptions): Provider {
        if (options.useFactory) {
            return {
                inject: options.inject || [],
                provide: ENCRYPTION_OPTIONS,
                useFactory: options.useFactory,
            };
        }

        return {
            inject: [options.useExisting || options.useClass],
            provide: ENCRYPTION_OPTIONS,
            useFactory: async (optionsFactory: EncryptionOptionsFactory) =>
              await optionsFactory.getOptions(),
        };
    }

    /**
     * Метод регистрации модуля.
     *
     * @param options Опции регистрации.
     */
    public static registerAsync (options: EncryptionAsyncOptions): DynamicModule {
        return {
            exports: [EncryptionService],
            imports: options.imports || [],
            module: EncryptionModule,
            providers: EncryptionModule.createAsyncProviders(options),
        };
    }
}
