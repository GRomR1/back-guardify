import { Type } from '@nestjs/common';
import { ModuleMetadata } from '@nestjs/common/interfaces';

export const ENCRYPTION_OPTIONS = 'ENCRYPTION_OPTIONS';

export interface EncryptionOptions {
    algorithm: string;
    key: Buffer;
    iv: Buffer;
}

export interface EncryptionOptionsFactory {
    getOptions(): EncryptionOptions | Promise<EncryptionOptions>;
}

export interface EncryptionAsyncOptions extends Pick<ModuleMetadata, 'imports'> {
    useFactory?: (...args: any[]) => Promise<EncryptionOptions> | EncryptionOptions;
    inject?: any[];
    useClass?: Type<EncryptionOptionsFactory>;
    useExisting?: Type<EncryptionOptionsFactory>;
}
