import { Inject, Injectable } from '@nestjs/common';
import { createCipheriv, createDecipheriv } from 'crypto';

import { EncryptionOptions, ENCRYPTION_OPTIONS } from './encryption.entity';

/**
 * Сервис шифрования.
 *
 * key, iv should be generated in this way
 *
 * const iv = crypto.randomBytes(16).toString('hex');
 * const key = crypto.randomBytes(32).toString('hex');
 */
@Injectable()
export class EncryptionService {
    constructor(@Inject(ENCRYPTION_OPTIONS) private readonly encryptionOptions: EncryptionOptions) {}

    /**
     * Метод шифрования текста.
     *
     * @param text the text message to be encrypted.
     *
     * @returns encrypted text message.
     */
    public encrypt(text: string): string {
        const { algorithm, iv, key } = this.encryptionOptions;
        const cipher = createCipheriv(algorithm || 'aes-256-cbc', Buffer.from(key), iv);
        let encrypted = cipher.update(text);
        encrypted = Buffer.concat([encrypted, cipher.final()]);

        return encrypted.toString('hex');
    }

    /**
     * Метод расшифровки текста.
     *
     * @param text the text message to be decrypted
     * @returns the decrypted text message
     */
    public decrypt(text: string): string {
        const { algorithm, iv, key } = this.encryptionOptions;
        const encryptedText = Buffer.from(text, 'hex');
        const decipher = createDecipheriv(algorithm || 'aes-256-cbc', Buffer.from(key), iv);
        let decrypted = decipher.update(encryptedText);
        decrypted = Buffer.concat([decrypted, decipher.final()]);

        return decrypted.toString();
    }
}
